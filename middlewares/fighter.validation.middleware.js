const {isEmpty, typeData, minNum, maxNum} = require('./validation');
const FighterService = require("../services/fighterService");

const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    const data = req.body;
    const errors = validFighter(data);

    if (errors.length > 0) {
        errors.push('-------------------------');
        const message = errors.forEach( error => console.log(error));
        errors.length = 0;

        return res.status(400).json({
            error: true,
            message
        })
    } else if (FighterService.searchFighter({name: req.body.name})) {
        res.err = Error(`User entity to create isn't valid. Fighter exist`);
        res.err.status = 400;
    }

    next();
};

const updateFighterValid = (req, res, next) => {

    const data = req.body;
    const errors = validFighter(data);
    fighter.updateFighter()

    if (errors.length > 0) {

        const message = errors.forEach( error => console.log(error));
        errors.length = 0;

        return res.status(400).json({
            error: true,
            message
        })
    }

    next();
};

const validFighter = (fighter) => {
    let errors = [];

    if (isEmpty(fighter.name)) {
        errors.push(`Field 'name' is empty!`);
    }
    // if (isEmpty(fighter.health)) {
    //     errors.push(`Field 'health' is empty!`);
    // } else if (typeData(Number(fighter.health))) {
    //     errors.push("'Health' is not number!");
    // }
    if (isEmpty(fighter.power)) {
        errors.push(`Field 'power' is empty!`);
    } else if (!typeData(Number(fighter.power))) {
        errors.push("'Power' is not number!");
    } else if (!maxNum(fighter.power, 100)) {
        errors.push("'Power' cannot be greater than 100");
    } else if (fighter.power === 0) {
        errors.push("'Power' cannot be equal to 0");
    } else if (!minNum(fighter.power, 0)) {
        errors.push("'Power' cannot be less than 0")
    }
    if (isEmpty(fighter.defense)) {
        errors.push("'Defense' is not number!");
    } else if (!typeData(Number(fighter.defense))) {
        errors.push("'Defense' is not number!");
    } else if (!maxNum(fighter.defense, 9) ||
        !minNum(fighter.defense, 1)) {
        errors.push("'Defense' should be between 1 and 9");
    }

    return errors;
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;